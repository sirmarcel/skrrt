"""Experimental evaluator that refines KRR parameters."""

from cmlkit import load_dataset, from_config

from cmlkit.evaluation.evaluator import Evaluator
from cmlkit.evaluation.loss import get_lossf
from cmlkit.representation import Composed
from cmlkit.utility import convert, unconvert, timed
from cmlkit.engine import parse_config


class SkrrtEvaluator(Evaluator):
    """Skrrt Evaluator.

    Evaluator that refines a KRR model using a "tuner".
    At the moment only one such "tuner" exists, and it
    uses a local grid search and a cross validation loss.

    The Evaluator takes care of computing the representation,
    which only needs to be done once, and formatting the output
    in the way expected by `cmlkit.tune`.

    Parameters:
        data: Name of a Dataset, or Dataset
        tuner: Tuner instance or config (see `krr_tuner.py`)
        target: Property in Dataset

    """

    kind = "eval_skrrt"

    def __init__(self, data, tuner, target, context={}):
        super().__init__(context=context)

        self.data = load_dataset(data)
        self.tuner = from_config(tuner, context=self.context)
        self.target = target

    def _get_config(self):
        return {
            "data": self.data.name,
            "tuner": self.tuner.get_config(),
            "target": self.target,
        }

    def __call__(self, config):
        result, duration = timed(self.evaluate)(config)
        result["duration"] = duration

        return result

    def evaluate(self, config):
        kind, inner = parse_config(config)
        representation = inner["representation"]
        per = inner["per"]

        if isinstance(representation, (list, tuple)):
            rep = Composed(*representation, context=self.context)
        else:
            rep = from_config(representation, context=self.context)

        x = rep(self.data)

        y = self.data.pp(self.target, per=per)

        config_krr, loss, result = self.tuner.tune(
            x=x, y=y, idx=self.data.splits, config=inner["regression"]
        )

        refined_suggestion = {
            "model": {
                "representation": rep.get_config(),
                "per": per,
                "regression": config_krr,
            }
        }

        return {"loss": loss, "refined_suggestion": refined_suggestion}
