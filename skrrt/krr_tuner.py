"""Infrastructure to tune KRR parameters."""

import qmmlpack
from functools import partial
import numpy as np

from cmlkit.engine import Component, parse_config
from cmlkit.engine.caching import memcached
from cmlkit.evaluation import get_lossf
from cmlkit import from_config


class KRRTunerCV(Component):
    """Refines KRR models with cross-validation.

    The Tuner expects a config corresponding to a
    `cmlkit.regression.qmml` regressor, with the `nl`
    and `ls` parameters not yet given, but replaced by
    the specification of a search space of the from expected
    by the `cmlkit.utility.opt_lgs` optimiser, or rather the
    `qmmlpack.local_grid_search` method. It is simply a list:

    [value, priority, stepsize, minimum, maximum, direction, base]

    Here is the explanation, copied over from `qmmlpack`:

    Index Name        Description   Default     Explanation
      0   'value'     initial value   0.        initial value; real number; exponent to base
      1   'priority'  priority        1         positive integer; higher priorities are optimized before lower ones; several variables may have same priority
      2   'stepsize'  step size       1.        positive real number; value (exponent) changes by this amount
      3   'minimum'   minimum value   -infinity lowest allowed value for variable, value in [minimum,maximum]
      4   'maximum'   maximum value   +infinity highest allowed value for variable, value in [minimum,maximum]
      5   'direction' direction       0         either -1 (small values preferred), 0 (indifferent), +1 (large values preferred)
      6   'base'      base            2.        positive real number

    So for instance [0, 1, 0.5, -20, 2, -1, 2.0] is:
        value = 0
        priority = 1
        stepsize = 0.5
        minimum = -20
        maximum = 2
        direction = -1 (negative)
        base = 2

    Reasonable starting points are:
        nl: (0, 1, 0.5, -20, 2, -1, 2.0)
        ls: (0, 2, 0.5, -15, 15, +1, 2.0)

    For caching, pass the following as context:
        {"cache": {"mem": {"max_size": 10}}}

    `max_size` is the maximum number of kernel matrices to be cached.
    Please note that this can get quite memory intensive, so keep an eye on that.

    Also note that we implicitly assume the cross-validation splits
    to mostly cover the dataset. If this is not the case, computing the
    whole-dataset kernel matrix is not efficient!

    Parameters:
        optimizer: Config of an optimiser, expects OptimizerLGS at the moment
        lossf: String identifying a loss function


    """

    kind = "krr_tuner_cv"

    default_context = {"cache": None}

    def __init__(self, optimizer, lossf="rmse", context={}):
        super().__init__(context=context)

        self.optimizer = from_config(optimizer, context=context)
        self.lossf = get_lossf(lossf)

        if self.context["cache"] is not None:
            kind, inner = parse_config(self.context["cache"])
            assert kind == "mem", f"KRRTunerCV does not support cache type {kind}."
            assert "max_entries" in inner, f"Cache config must contain max_entries"

    def _get_config(self):
        return {
            "centering": self.centering,
            "optimizer": self.optimizer.get_config(),
            "lossf": self.lossf.__name__,
        }

    def prepare_krr(self, model):
        self.args_nl, self.args_ls, self.centering, self.make_kernel = parse_krr_config(
            model, context=self.context
        )

    def make_target(self, x, y, idx):

        n_splits = len(idx)

        def kernel(ls):
            actual_kernel = self.make_kernel(ls)
            return actual_kernel(x)

        if self.context["cache"] is not None:
            _, inner = parse_config(self.context["cache"])
            kernel = memcached(kernel, max_entries=inner["max_entries"])

        def target(nl, ls):

            kernel_matrix = kernel(ls)

            loss = 0.0
            for train, test in idx:
                kernel_train = kernel_matrix[np.ix_(train, train)]
                kernel_test = kernel_matrix[np.ix_(train, test)]

                krr = qmmlpack.KernelRidgeRegression(
                    kernel_train, y[train], theta=(nl,), centering=self.centering
                )

                pred = krr(kernel_test)
                true = y[test]

                loss += self.lossf(true, pred) / n_splits

            return loss

        return target

    def tune(self, x, y, idx, config):
        self.prepare_krr(config)
        target = self.make_target(x, y, idx)

        result = self.optimizer(target, (self.args_nl, self.args_ls))

        config_krr = {
            "krr": {
                "centering": self.centering,
                "nl": result["best"][0],
                "kernel": self.make_kernel(result["best"][1]).get_config(),
            }
        }

        loss = result["best_f"]

        return config_krr, loss, result


def parse_krr_config(config, context={}):
    k, inner_krr = parse_config(config)
    assert (
        k == "krr"
    ), "KRR Tuner can only tune qmmlpack-style KRR models (at the moment)."

    kind_kernel, inner_kernel = parse_config(inner_krr["kernel"])
    kind_kernelf, inner_kernelf = parse_config(inner_kernel["kernelf"])

    assert (
        "ls" in inner_kernelf
    ), "KRR Tuner expects a kernel function with a ls parameter"

    args_nl = inner_krr["nl"]
    args_ls = inner_kernelf["ls"]
    centering = inner_krr.get("centering", False)

    def make_kernel(ls):
        return from_config(
            {kind_kernel: {**inner_kernel, "kernelf": {kind_kernelf: {"ls": ls}}}},
            context=context,
        )

    return args_nl, args_ls, centering, make_kernel
