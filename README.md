# skrrt 🚗💨 

*specialised krr tuner [skrrt](https://www.youtube.com/watch?v=pzhctd7n_fA) [skrrt](https://www.urbandictionary.com/define.php?term=skrrt) [skrrt](https://www.youtube.com/watch?v=2XnojwxGRVY)*

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/python/black) 

`skrrt` is an experimental [`cmlkit`](https://marcel.science/cmlkit) plugin to fine-tune kernel ridge regression models. It was developed for the [`repbench`](https://marcel.science/repbench) project.

Technically speaking, `skrrt` performs a local grid search on a cross-validation loss in order to optimise the `ls` and `nl` parameters of a KRR model. It is typically used as an `Evaluator` for `cmlkit.tune`, where the stochastic search is responsible for picking starting points, and `skrrt` then tries to get closer to a local minimum. It then emits a `refined_suggestion` with a model config.

`skrrt` makes some effort to be efficient: The representation is only computed once, and the CV is done via slicing the total kernel matrix, rather then re-computing redundant entries. The kernel matrix is also cached.

Documentation can be found in the code.

## Installation

Due to its experimental nature, `skrrt` is not published to the python package index. Instead, simply clone this repository, or add it as a `git` dependency to `poetry`.

Then, add it to `PYTHONPATH`.

Then, export `CML_PLUGINS=skrrt` or add `skrrt` to an already existing plugin list.
