from unittest import TestCase
import numpy as np
import pathlib
import shutil

from cmlkit.regression.qmml import KRR
from cmlkit.utility.indices import twoway_split
from cmlkit import from_config

from skrrt.krr_tuner import KRRTunerCV


def f(x):
    return x.flatten() ** 3


def rmse(true, pred):
    return np.sqrt(np.mean((true - pred) ** 2))


class TestKRRTuner(TestCase):
    def setUp(self):
        self.x_train = 16 * np.random.random((80, 1)) - 8
        self.x_test = 16 * np.random.random((20, 1)) - 8

        self.y_train = f(self.x_train)
        self.y_test = f(self.x_test)

        self.idx = [twoway_split(80, 20) for i in range(3)]

        self.tuner = KRRTunerCV(
            optimizer={"opt_lgs": {"resolution": None, "rng": 123, "maxevals": 50}},
            context={"kernel_global": {"sentinel": True}},
        )

        baseline = KRR(
            kernel={"kernel_global": {"kernelf": {"gaussian": {"ls": 8.0}}}}, nl=1.0
        )
        baseline.train(x=self.x_train, y=self.y_train)
        pred = baseline.predict(z=self.x_test)

        self.baseline_loss = rmse(self.y_test, pred)

    def test_does_it_improve(self):

        init_config = {
            "krr": {
                "nl": (0, 1, 0.5, -20, 2, -1, 2.0),
                "kernel": {
                    "kernel_global": {
                        "kernelf": {"gaussian": {"ls": (3.0, 2, 0.5, -15, 15, +1, 2.0)}}
                    }
                },
            }
        }

        config, loss, result = self.tuner.tune(
            x=self.x_train, y=self.y_train, idx=self.idx, config=init_config
        )

        krr = from_config(config)
        krr.train(x=self.x_train, y=self.y_train)
        pred = krr.predict(z=self.x_test)

        loss = rmse(self.y_test, pred)

        self.assertLess(loss, self.baseline_loss)

        # check that the context gets passed correctly
        kernel = self.tuner.make_kernel(1.0)
        self.assertTrue("sentinel" in kernel.context)
