from unittest import TestCase
import numpy as np
import pathlib
import shutil

from cmlkit.engine import read_yaml

from skrrt.krr_tuner import KRRTunerCV
from skrrt.evaluator import SkrrtEvaluator


class TestSkrrtEvaluator(TestCase):
    def test_basic(self):
        tuner = KRRTunerCV(
            optimizer={"opt_lgs": {"resolution": None, "rng": 123, "maxevals": 20}}
        )
        evaluator = SkrrtEvaluator(data="kaggle-cv", tuner=tuner, target="fe")

        model = read_yaml("space")

        result = evaluator(model)

        print(result)

        self.assertTrue("loss" in result)
        self.assertTrue("refined_suggestion" in result)

        tuner = KRRTunerCV(
            optimizer={"opt_lgs": {"resolution": None, "rng": 123, "maxevals": 20}},
            context={"cache": {"mem": {"max_entries": 20}}}
        )
        evaluator = SkrrtEvaluator(data="kaggle-cv", tuner=tuner, target="fe")

        model = read_yaml("space")

        result2 = evaluator(model)

        self.assertLess(result2["duration"], result["duration"])
