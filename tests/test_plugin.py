from unittest import TestCase

import cmlkit

# this test assumes that you have somewhere exported
# CML_PLUGINS=skrrt


class TestPluginSystem(TestCase):
    def test_basic(self):
        assert "eval_skrrt" in cmlkit.classes
        assert "krr_tuner_cv" in cmlkit.classes
